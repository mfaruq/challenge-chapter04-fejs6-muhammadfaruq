import React from 'react'
import { Routes, Route } from "react-router-dom";

import { Template } from '../Views/Templates/Template';
import { Search } from '../Views/Templates/Search';
import { Detail } from '../Views/Templates/Detail';
import { Tes } from '../Views/Templates/Tes';

export const Routers = () => {
    return (
        <Routes>
            <Route path="/Template" element={<Template />} />
            <Route path="/Search" element={<Search />} />
            <Route path="/Detail" element={<Detail />} />
            <Route path="/Tes" element={<Tes />} />
        </Routes>
    )
};
