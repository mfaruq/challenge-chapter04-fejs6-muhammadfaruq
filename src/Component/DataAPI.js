import React, { useState, useEffect } from 'react'
import axios from 'axios'
import '../Views/Templates/style.css'

import User from "../Assets/img/fi_users.png";
import Setting from "../Assets/img/fi_settings.png";
import Calendar from "../Assets/img/fi_calendar.png";

const DataAPI = () => {
    const [post, setPost] = useState([])

    useEffect(() => {
        axios.get('https://rent-cars-api.herokuapp.com/admin/car')
            // axios.get('https://jsonplaceholder.typicode.com/posts')
            .then(res => {
                console.log(res)
                setPost(res.data)
            })
            .catch(err => {
                console.log(err)
            })
    })

    return (
        <div>
            <div className='row mf-api-row container'>
                {
                    post.map(post =>
                        <div className='col-lg-4 mb-3'>
                            <div className='card'>
                                <div className='card-body'>
                                    <img src={post.image} className='card-img mt-5 mb-5' />
                                    <p>
                                        {post.name}
                                    </p>
                                    <p>
                                        {post.status}
                                    </p>
                                    <p><strong>
                                        Rp &ensp;
                                        {post.price}
                                    </strong></p>
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                    </p>
                                    <p>
                                        <img src={User} alt="" className='' />&emsp;4 Orang
                                    </p>
                                    <p>
                                        <img src={Setting} alt="" className='' />&emsp;Manual
                                    </p>
                                    <p>
                                        <img src={Calendar} alt="" className='' />&emsp;Tahun 2022
                                    </p>
                                    <button type='button' className='btn btn-success btn-sm mf-btn-pembayaran'>Pilih Mobil</button>
                                </div>
                            </div>
                        </div>
                    )
                }
            </div>
        </div>
    )
}

export default DataAPI