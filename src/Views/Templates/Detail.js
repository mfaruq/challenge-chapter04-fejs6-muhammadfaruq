import React from 'react'

import Rectangle from "../../Assets/img/Rectangle.png";
import CarSearch from "../../Assets/img/car.png";
import Facebook from "../../Assets/img/icon_facebook.png";
import Instagram from "../../Assets/img/icon_instagram.png";
import Twitter from "../../Assets/img/icon_twitter.png";
import Mail from "../../Assets/img/icon_mail.png";
import Twitch from "../../Assets/img/icon_twitch.png";
import User from "../../Assets/img/fi_users.png";
import Setting from "../../Assets/img/fi_settings.png";
import Calendar from "../../Assets/img/fi_calendar.png";

import './style.css'

export const Detail = () => {
    return (
        <div>

            <div className='row'>
                <div className='col-lg-12 mf-row-2'>
                    <div className='row'>
                        <div className='col-lg-4'>
                            <img src={Rectangle} alt="" className='mf-img-rectangle' />
                        </div>
                        <div className='col-lg-8 d-flex justify-content-end'>
                            <a href='#' className='mf-a-navbar'>Our Services</a>
                            <a href='#' className='mf-a-navbar'>Why Us</a>
                            <a href='#' className='mf-a-navbar'>Testimonial</a>
                            <a href='#' className='mf-a-navbar'>FAQ</a>
                            <button type="button" className='btn btn-success btn-sm mf-btn-register'>Register</button>
                        </div>
                    </div>
                </div>
            </div>

            <div className='row mf-row-search-search shadow-sm'>
                <div className='col-lg-12'>
                    <div className='row mf-row-form'>
                        <div className='col-lg-12'>
                            <form>
                                <label className='mf-form-label'>Tipe Driver</label>
                                <label className='mf-form-label'>Tanggal</label>
                                <label className='mf-form-label'>Waktu Jemput/Ambil</label>
                                <label className='mf-form-label'>Jumlah Penumpang (optional)</label>
                                <br />
                                <input id='tipeDriver' type='text' className='mf-form-input' />
                                <input id='tanggal' type='date' className='mf-form-input' />
                                <input id='waktuJemput' type='time' className='mf-form-input' />
                                <input id='jumlahPenumpang' type='number' className='mf-form-input' />
                                {/* <input type='button' value='Cari Mobil' className='btn btn-success mf-btn-search' /> */}
                                <button type='submit' className='btn btn-success mf-btn-search'>Cari Mobil</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div className='row mf-row-content-search'>

                <div className='col-lg-7 mb-3'>
                    <div className='card'>
                        <div className='card-body'>
                            <p><strong>Tentang Paket</strong></p>
                            <p>Include</p>
                            <ul className='mf-ul'>
                                <li>Apa saja yang termasuk dalam paket misal durasi max 12 jam</li>
                                <li>Sudah termasuk bensin selama 12 jam</li>
                                <li>Sudah termasuk Tiket Wisata</li>
                                <li>Sudah termasuk pajak</li>
                            </ul>
                            <p>Exclude</p>
                            <ul className='mf-ul'>
                                <li>Tidak termasuk biaya makan sopir Rp 75.000/hari</li>
                                <li>Jika overtime lebih dari 12 jam akan ada tambahan biaya Rp 20.000/jam</li>
                                <li>Tidak termasuk akomodasi penginapan</li>
                            </ul><br />
                            <p><strong>Refund, Reschedule, Overtime</strong></p>
                            <ul className='mf-ul'>
                                <li>Tidak termasuk biaya makan sopir Rp 75.000/hari</li>
                                <li>Jika overtime lebih dari 12 jam akan ada tambahan biaya Rp 20.000/jam</li>
                                <li>Tidak termasuk akomodasi penginapan</li>
                                <li>Tidak termasuk biaya makan sopir Rp 75.000/hari</li>
                                <li>Jika overtime lebih dari 12 jam akan ada tambahan biaya Rp 20.000/jam</li>
                                <li>Tidak termasuk akomodasi penginapan</li>
                                <li>Tidak termasuk biaya makan sopir Rp 75.000/hari</li>
                                <li>Jika overtime lebih dari 12 jam akan ada tambahan biaya Rp 20.000/jam</li>
                                <li>Tidak termasuk akomodasi penginapan</li>
                            </ul>
                        </div>
                    </div>
                    <button type='button' className='btn btn-success btn-sm mf-btn-pembayaran mt-3'>Lanjutkan Pembayaran</button>
                </div>
                <div className='col-lg-5'>
                    <div className='card'>
                        <div className='card-body'>
                            <img src={CarSearch} alt="" className='card-img mf-img-car-search' />
                            <p>Nama/Tipe Mobil</p>
                            <p><strong>Rp 430.000 / hari</strong></p>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                            </p>
                            <p>
                                <img src={User} alt="" className='' />&emsp;4 Orang
                            </p>
                            <p>
                                <img src={Setting} alt="" className='' />&emsp;4 Orang
                            </p>
                            <p>
                                <img src={Calendar} alt="" className='' />&emsp;4 Orang
                            </p>
                            <button type='button' className='btn btn-success btn-sm mf-btn-pembayaran'>Lanjutkan Pembayaran</button>
                        </div>
                    </div>
                </div>

            </div>

            <div className='row mf-footer'>
                <div className='col-lg-3'>
                    <p>Jalan Suroyo No. 161 Mayangan Kota Probolonggo 672000</p>
                    <p>binarcarrental@gmail.com</p>
                    <p>081-233-334-808</p>
                </div>
                <div className='col-lg-3'>
                    <p>Our Services</p>
                    <p>Why Us</p>
                    <p>Testimonial</p>
                    <p>FAQ</p>
                </div>
                <div className='col-lg-3'>
                    <p>Connect with us</p>
                    <img src={Facebook} alt="" className='mf-img-footer' />
                    <img src={Instagram} alt="" className='mf-img-footer' />
                    <img src={Twitter} alt="" className='mf-img-footer' />
                    <img src={Mail} alt="" className='mf-img-footer' />
                    <img src={Twitch} alt="" className='mf-img-footer' />
                </div>
                <div className='col-lg-3'>
                    <p>Copyright Binar 2022</p>
                    <img src={Rectangle} alt="" className='mf-img-footer-rec' />
                </div>
            </div>

        </div>
    )
}
