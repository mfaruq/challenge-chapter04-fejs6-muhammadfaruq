import React from 'react'

import Rectangle from "../../Assets/img/Rectangle.png";
import CarSearch from "../../Assets/img/car.png";
import Facebook from "../../Assets/img/icon_facebook.png";
import Instagram from "../../Assets/img/icon_instagram.png";
import Twitter from "../../Assets/img/icon_twitter.png";
import Mail from "../../Assets/img/icon_mail.png";
import Twitch from "../../Assets/img/icon_twitch.png";
import User from "../../Assets/img/fi_users.png";
import Setting from "../../Assets/img/fi_settings.png";
import Calendar from "../../Assets/img/fi_calendar.png";

import './style.css'
import DataAPI from '../../Component/DataAPI';

export const Search = () => {
    return (
        <div>

            <div className='row'>
                <div className='col-lg-12 mf-row-2'>
                    <div className='row'>
                        <div className='col-lg-4'>
                            <img src={Rectangle} alt="" className='mf-img-rectangle' />
                        </div>
                        <div className='col-lg-8 d-flex justify-content-end'>
                            <a href='#' className='mf-a-navbar'>Our Services</a>
                            <a href='#' className='mf-a-navbar'>Why Us</a>
                            <a href='#' className='mf-a-navbar'>Testimonial</a>
                            <a href='#' className='mf-a-navbar'>FAQ</a>
                            <button type="button" className='btn btn-success btn-sm mf-btn-register'>Register</button>
                        </div>
                    </div>
                </div>
            </div>

            <div className='row mf-row-search-search shadow-sm'>
                <div className='col-lg-12'>
                    <div className='row mf-row-form'>
                        <div className='col-lg-12'>
                            <form>
                                <label className='mf-form-label'>Tipe Driver</label>
                                <label className='mf-form-label'>Tanggal</label>
                                <label className='mf-form-label'>Waktu Jemput/Ambil</label>
                                <label className='mf-form-label'>Jumlah Penumpang (optional)</label>
                                <br />
                                <select className='mf-form-input'>
                                    <option>Pilih Tipe Driver</option>
                                    <option value="tipeTrue">Dengan Sopir</option>
                                    <option value="tipeFalse">Tanpa Sopir (Lepas Kunci)</option>
                                </select>
                                <input id='tanggal' type='date' className='mf-form-input' />
                                <input id='waktuJemput' type='time' className='mf-form-input' />
                                <input id='jumlahPenumpang' type='number' className='mf-form-input' />
                                {/* <input type='button' value='Cari Mobil' className='btn btn-success mf-btn-search' /> */}
                                <button type='submit' className='btn btn-success mf-btn-search'>Cari Mobil</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div className='row mf-row-content-search'>
                <DataAPI />
            </div>

            <div className='row mf-footer'>
                <div className='col-lg-3'>
                    <p>Jalan Suroyo No. 161 Mayangan Kota Probolonggo 672000</p>
                    <p>binarcarrental@gmail.com</p>
                    <p>081-233-334-808</p>
                </div>
                <div className='col-lg-3'>
                    <p>Our Services</p>
                    <p>Why Us</p>
                    <p>Testimonial</p>
                    <p>FAQ</p>
                </div>
                <div className='col-lg-3'>
                    <p>Connect with us</p>
                    <img src={Facebook} alt="" className='mf-img-footer' />
                    <img src={Instagram} alt="" className='mf-img-footer' />
                    <img src={Twitter} alt="" className='mf-img-footer' />
                    <img src={Mail} alt="" className='mf-img-footer' />
                    <img src={Twitch} alt="" className='mf-img-footer' />
                </div>
                <div className='col-lg-3'>
                    <p>Copyright Binar 2022</p>
                    <img src={Rectangle} alt="" className='mf-img-footer-rec' />
                </div>
            </div>

        </div>
    )
}
