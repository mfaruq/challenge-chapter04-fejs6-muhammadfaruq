import React, { useState, useEffect } from 'react'
import axios from 'axios'

const Data = () => {
    const [post, setPost] = useState([])

    useEffect(() => {
        // axios.get('https://jsonplaceholder.typicode.com/posts')
        axios.get('https://rent-cars-api.herokuapp.com/admin/car')
            .then(res => {
                console.log(res)
                setPost(res.data)
            })
            .catch(err => {
                console.log(err)
            })
    })

    return (
        <div>
            <div className='row'>
                {
                    post.map(post =>
                        <div className='col-lg-4'>
                            <div className='card'>
                                <div className='card-body'>
                                    {post.name}
                                </div>
                            </div>
                        </div>
                    )
                }
            </div>
            {/* <div className='card'>
                <div className='card-body'>
                    {
                        post.map(post => post.name)
                    }
                </div>
            </div> */}
            {/* <ul>
                {
                    post.map(post => <li key={post.id}>{post}</li>)
                }
            </ul> */}
        </div>
    )
}

export default Data